const defaultSorts = (el1: any, el2: any) => true

export default class NonMutator {
	static copyWithin(
		arr: Array<any>,
		target: number,
		start: number = 0,
		end: number = arr.length
	) {}

	static fill(
		arr: Array<any>,
		value: any,
		start: number = 0,
		end: number = arr.length
	) {}

	static pop(arr: Array<any>) {}

	static push(arr: Array<any>, element: any) {}

	static reverse(arr: Array<any>) {}

	static shift(arr: Array<any>) {}

	static sort(
		arr: Array<any>,
		compareFun: (el1: any, el2: any) => boolean = defaultSorts
	) {}

	static splice(
		arr: Array<any>,
		start: number,
		deleteCount: number = 0,
		itemToAdd: any = null
	) {}

	static unshift(arr: Array<any>, elementToUnshift: any) {}
}
